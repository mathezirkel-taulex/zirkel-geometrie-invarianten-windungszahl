% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\renewcommand{\schuljahr}{2023/24}
\renewcommand{\klasse}{9}
\renewcommand{\titel}{Spaß mit Schleifen und ihren Invarianten (Teil 2/2)} 
\renewcommand{\untertitel}{Präsenzzirkel \#5 vom 11. Januar 2024}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheader

    \vspace*{-2.5em}


    \setcounter{section}{1}
    \setcounter{aufg}{2}

    \section{Windungszahl}
    \vspace*{-0.5em}

    Folgende praktische Definition wird uns helfen, die Aufgaben~1 und~2 einfacher zu lösen.

    \begin{definition}[Windungszahl um den Ursprung]\label{def:windingzero}
        Die \emph{Windungszahl}~$\omega_0(K) \in \mathbb{Z}$ einer Schleife~$K$ ist die Anzahl an Umdrehungen einer Schleife um den Ursprung~$0$ herum.
    \end{definition}

    \begin{aufgabe}
        Ermittle die Windungszahl jeder Kurve aus Abbildung~\ref{fig:exwinding} um ihren jeweiligen Ursprung -- jeweils markiert durch das Kreuzchen. Was fällt dir auf? % fig:exwinding jeweils zeilenweise von links, Orientierungen ohne Einschränkung so gewählt, dass die Windungszahl positiv ist. Zeile 1: 1, 3, 2, 0, 2. Zeile 2: 2, 0, 3, 1. Zeile 3: 1, 0, 2
    \end{aufgabe}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.6\linewidth]{geometrie-invarianten/loops-winding.pdf}
        \caption{Beispiele für Schleifen vom letzten Mal.}\label{fig:exwinding}
    \end{figure}
    \vspace*{-0.75em}

    \begin{hinweis}
        \, \vspace*{-0.25em}
        \begin{itemize}
            \item Definition~\ref{def:windingzero} ist nur sinnvoll, wenn $0 \notin K .$ \vspace*{-0.5em}
            \item Die Drehungen werden im üblichen mathematischen Drehsinn des zweidimensionalen Koordinatensystems gezählt. \vspace*{-0.5em}
            \item Die Windungszahl kann auch für nicht geschlossene Kurven definiert werden.
        \end{itemize}
    \end{hinweis}

    \enlargethispage{3.5\baselineskip}

    Wir nehmen nun an, dass Schleifen beim Verformen auch durch den Ursprung bewegt werden dürfen.

    \begin{aufgabe}
        Seien $K_1$ und $K_2$ zwei Schleifen mit ungleicher Windungszahl um den Ursprung, d.h. es gilt: $\omega_0(K_1) \neq \omega_0(K_2) .$ Wenn wir $K_1$ zu $K_2$ verformen, muss sich also irgendwann die Windungszahl ändern. Findest du heraus, unter welchen Bedingungen sich die Windungszahl während der Verformung ändert? % wenn bei der Verformung der Ursprung überquert wird.
    \end{aufgabe}




    \newpage

    \vspace*{-7em}

    \section{Zusammenhangskomponenten}

    Bei der Analyse von Schleifen können wir nicht nur die Schleife selbst und ihre Schnittpunkte betrachten, sondern auch die Teile, in die die Schleife die Ebene zerteilt.

    \begin{definition}[Zusammenhangskomponente]
        Eine Zusammenhangskomponente $C$ ist eine maximale, zusammenhängende Teilmenge eines euklidischen Raums $X .$ Eine Teilmenge $C$ ist \emph{zusammenhängend}, wenn für alle Punkte in $C$ gilt, dass sie durch eine Kurve in $C$ verbunden werden können. Und \emph{maximal}, wenn für alle $D \subseteq X$ mit $C \subsetneq D$ gilt: $D$ ist nicht zusammenhängend.
    \end{definition}
    \vspace*{-0.5em}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.4\linewidth]{geometrie-invarianten/kol-leoniecomps1.pdf}
        \hspace*{5em}
        \includegraphics[width=0.4\linewidth]{geometrie-invarianten/kol-leonie-dots.pdf}
        \caption{Schleife links: gefärbte Zusammenhangskomponenten; rechts: ausgewählte Punkte.}\label{fig:loopleo}
    \end{figure}

    Wir betrachten also die Zusammenhangskomponenten von $\mathbb{R}^2 \setminus K .$ Häufig bezeichnen wir diese umgangssprachlich als die ``Zusammenhangskomponenten von $K$'', obwohl das genauer betrachtet wenig Sinn ergibt, denn eine Schleife $K$ besteht selbst aus nur einer Zusammenhangskomponente -- der Schleife selbst.

    \begin{aufgabe}
        Zähle für die Schleife in Abbildung~\ref{fig:loopleo} die Anzahl der Zusammenhangskomponenten. % 10 Zusammenhangskomponenten (die unbeschränkte nicht vergessen!)

        \textbf{Bonus:} Wie viele Zusammenhangskomponenten hat eine Schleife $K ,$ wenn alle ihre Schnittpunkte nur Doppelpunkte sind -- also z.B. keine dreifachen Schnittpunkte -- und sie $n_K$ viele Schnittpunkte hat? Teste dein Ergebnis auch an den Schleifen aus Abbildung~\ref{fig:exwinding}.\\ % Anzahl an Zshk'en ist gleich n_K + 2
        Wie könntest du mit mehr als doppelten Schnittpunkten klarkommen? % Seien n_j die j-fachen Doppelpunkte einer Schleife. Dann ist die Anzahl der Zshk'en gleich 2 + \sum \limits_{j=2}^{\infty} n_j (j - 1) -> d.h. wir haben 2 Komponenten von Anfang an, und dazu pro j-fachem Doppelpunkt (j-1) viele Komponenten zusätzlich.
    \end{aufgabe}


    Jetzt sollten wir den Begriff der Windungszahl auch auf beliebige Punkte ausweiten.

    \begin{definition}[Windungszahl um beliebigen Punkt]
        Die Windungszahl~$\omega_p(K)$ einer Schleife~$K$ ist die Anzahl an Umdrehungen einer Schleife um einen beliebigen Punkt~$p \notin K$ herum.
    \end{definition}

    \begin{aufgabe}
        Bestimme die Windungszahl für jeden Punkt im rechten Bild von Abbildung~\ref{fig:loopleo}. % von oben nach unten: 1, -1, 2, 0
    \end{aufgabe}

    \enlargethispage{3\baselineskip}

    Nach kurzer Überlegung stellen wir fest, dass die Windungszahl für alle Punkte innerhalb einer Zusammenhangskomponente gleich ist.
    Dadurch ist es sinnvoll, ganzen Zusammenhangskomponenten jeweils eine Windungszahl zuzuschreiben.

    \begin{aufgabe}
        Bestimme die Windungszahl für jede Zusammenhangskomponente links in Abbildung~\ref{fig:loopleo}. % die blauen haben 1, die orangene -1, die pinke 0, die weiße auch 0, die grünen 2.
    \end{aufgabe}




    \newpage

    \vspace*{-5em}


    \section{Querungen-Trick (für Windungszahlen)}
    \vspace*{-1em}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.6\linewidth]{geometrie-invarianten/windingstep.pdf}
    \end{figure}
    
    Beim Überqueren eines Kurvenstücks unterscheidet sich die Windungszahl der anliegenden Komponenten immer genau um $1$ oder $-1$, abhängig von der Orientierung des Kurvenstücks.


    \section{Einfache Anwendung: Punkt-in-Polygon (PIP)}
    \vspace*{-1em}

    % Wikipedia: In computational geometry, the point-in-polygon (PIP) problem asks whether a given point in the plane lies inside, outside, or on the boundary of a polygon. It is a special case of point location problems and finds applications in areas that deal with processing geometrical data, such as 
    % - computer graphics,
    % - computer vision,
    % - geographic information systems (GIS),
    % - motion planning,
    % - computer-aided design (CAD).
    % https://en.wikipedia.org/wiki/Point_in_polygon
    

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{geometrie-invarianten/pip.pdf}
        % \caption{Einfache Beispiele für das PIP-Problem.}
    \end{figure}
    \vspace*{-1em}


    \section{Rotationszahl und reguläre Verformungen}

    \enlargethispage{2\baselineskip}

    \begin{definition}[Rotationszahl]
        Die \emph{Rotationszahl}~$\operatorname{rot}(K) \in \mathbb{Z}$ einer Schleife~$K$ ist die Anzahl an Umdrehungen des Geschwindigkeitsvektors beim einmaligen Durchlaufen der Schleife.
    \end{definition}

    \begin{hinweis}
        \, \vspace*{-0.25em}
        \begin{itemize}
            \item Die Drehungen werden im üblichen mathematischen Drehsinn des zweidimensionalen Koordinatensystems gezählt. \vspace*{-0.25em}
            \item Die Rotationszahl kann auch für nicht geschlossene Kurven definiert werden. \vspace*{-0.25em}
            \item Die Rotationszahl ist nur definiert für \textbf{Kurven ohne (versteckte) Ecken}. Solche Kurven nennen wir auch \emph{reguläre Kurven}.
        \end{itemize}
    \end{hinweis}

    \begin{aufgabe}
        \vspace*{-1em}
        \begin{enumerate}[a)]
            \item Bestimme die jeweilige Rotationszahl für die Kurven aus Abbildungen~\ref{fig:exwinding} und~\ref{fig:loopleo}. % fig:exwinding jeweils zeilenweise von links, Orientierungen ohne Einschränkung so gewählt, dass die Rotationszahl positiv ist. Zeile 1: 0, 3, 2, 1, 2. Zeile 2: 2, 0, 3, 2. Zeile 3: 1, 0, 2; fig:loopleo hat Rotationszahl 3
            \item An welchen Stellen ändert sich die Rotationszahl einer Kurve, wenn wir sie verformen? % bei Ecken durch die Loopings entstehen oder verschwinden
            \item Unter welchen Bedingungen haben zwei Schleifen die gleiche Rotationszahl? % wenn sie regulär homotop sind, also wenn sie regulär ineinander verformbar sind (siehe unten)
        \end{enumerate}
    \end{aufgabe}




    \newpage

    \begin{definition}[Reguläre Verformung]
        Wir nennen die Verformung von einer Schleife zu einer anderen \emph{regulär}, wenn während der Verformung zu keinem Zeitpunkt Ecken existieren.
    \end{definition}

    Bei einer regulären Verformung können also keine Loopings entstehen oder verschwinden.\\
    \textbf{Bonus:} Das ist die erste der drei \emph{Reidemeister-Bewegungen}.

    \begin{satz}[\textbf{Bonus:} Satz von Whitney--Graustein]
        Zwei Schleifen haben genau dann die gleiche Rotationszahl, wenn sie regulär ineinander verformt werden können.
    \end{satz}

    \begin{hinweis}
        Die Rotationszahl ist Grundlage vieler anderer geometrischer Kennzahlen.
        Ohne reguläre Verformungen und ihre Eigenschaften (v.a. der \emph{Satz von Whitney--Graustein}) würden so gut wie alle Theoreme rund um reguläre Kurven einfach nicht funktionieren beziehungsweise nicht beweisbar sein.
        Eine praktische Anwendung ist zum Beispiel die $J^+$-Invariante, die Anhaltspunkte dafür gibt, ob ein im Weltraum umherfliegender Satellit ohne Kollision mit einem Planeten seinen Orbit zu einem neuen Wunschorbit ändern kann.
    \end{hinweis}

    \begin{hinweis}
        Was wir im ganzen Zirkel \emph{Verformung} genannt haben, nennt man üblicher auch \emph{Homotopie}. Kurven werden dann \emph{homotopiert}, statt \emph{verformt}. Und die Rotationszahl einer Schleife bleibt gleich unter \emph{regulärer Homotopie}.
    \end{hinweis}



    \section{Lächeln-Trick (für Rotationszahlen)}

    Zähle jedes richtige und umgedrehte Lächeln, die beim Durchlaufen der Kurve nach rechts (oder links) auftreten. Die Differenz ist die Rotationszahl (bis auf Vorzeichenfehler).

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.5\linewidth]{geometrie-invarianten/kol-leonie-rot-smiles-frowns.pdf}
        % \caption{}
    \end{figure}
    \vspace*{-1em}

    Ein anderer einfacher Trick, um die Rotationszahl einer komplizierten Kurve herauszufinden, ist es sie zu einer einfacheren Kurve regulär zu verformen. Da die Rotationszahl sich beim regulären Verformen nicht ändert, können wir dann einfach die Rotationszahl der einfacheren Kurve ermitteln.

    \vspace*{3em}
    \noindent\fbox{%
    \parbox{\textwidth}{%
        \textbf{Schauempfehlung} zum Thema Knotentheorie (leider nur englisch, ohne deutsche Untertitel): ``\emph{How The Most Useless Branch of Math Could Save Your Life}'' von \emph{Veritasium}, auf YouTube.
    }%
}
    
    % Für Fortsetzung: Reidemeister-Bewegungen und anderer Kram aus https://www.youtube.com/watch?v=8DBhTXM_Br4 evtl auch bisschen J+ falls spaßig möglich



    % \section{Bonus: Formale Definition einer Kurve}

    % \begin{definition}
    %     Eine Kurve ist die \emph{Bildmenge} von einem \emph{Weg}. Ein Weg ist eine \emph{stetige} Abbildung $q$ von einem einem \emph{reellen Intervall} $[a, b] \subset \mathbb{R}$ in einen {topologischen Raum} $X ,$ also
    %     $$q : [a, b] \to X ,$$
    %     wobei bei einem \emph{geschlossenen} Weg gilt, dass der Anfang $q(a)$ und das Ende $q(b)$ gleich sind, also $q(a) = q(b) ,$ und bei einer \emph{ebenen} Kurve ist $X = \mathbb{R}^2 .$
    % \end{definition}

    % Vor allem in früherer mathematischer Literatur wird oft auch der Weg selbst als Kurve bezeichnet. Das heißt sowohl die Abbildung selbst, als auch die Bildmenge wurde als \emph{Kurve} bezeichnet.\\
    % \\
    % Aus der Schule kennen wir übrigens schon beinahe fertig definierte Wege beziehungsweise Kurven, nämlich Graphen eindimensionaler Funktionen.
    % Ein Graph $G$ einer Funktion $f : D \to Z ,$ mit Definitionsmenge $D$ und Zielmenge $Z$ aus $\mathbb{R} ,$ ist die Menge
    % $$G = \{ (x, f(x)) \in \mathbb{R}^2 \, | \, x \in D \} $$
    % und das ist eine Kurve.
    % Das sehen wir leichter, wenn wir uns klarmachen, dass das die Bildmenge des folgenden Weges $g$ ist:
    % $$g: \mathbb{R} \to \mathbb{R}^2 , \quad \text{ mit } \quad g: x \to (x, f(x)) .$$


\end{document}