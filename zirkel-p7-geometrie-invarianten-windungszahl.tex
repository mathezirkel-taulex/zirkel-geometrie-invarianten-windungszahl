% Hier den Pfad zu zirkel.cls angeben
\documentclass{../tex-bilder/zirkel}

\graphicspath{{../tex-bilder/}}

\usepackage{mdframed}
\usepackage{array}      % for spacing in tabular

\renewcommand{\schuljahr}{2023/24}
\renewcommand{\klasse}{78a}
\renewcommand{\titel}{Spaß mit Schleifen} 
\renewcommand{\untertitel}{Präsenzzirkel \#4 vom 09. Januar 2024}
\renewcommand{\name}{Alexander Mai}

% Vordefinierte Umgebungen
%     * aufgabe
%     * hinweis
%     * loesung
%     * satz
%     * beispiel

\begin{document}
    \makeheader

    \vspace*{-1.5em}

    Eine geschlossene ebene Kurve bezeichnen wir als \emph{Schleife}. Eine Kurve darf keine Lücken haben, aber in der Regel einen Anfang und ein Ende. Weil sie \emph{geschlossen} ist, sind Anfang und Ende aber am selben Punkt.

    \vspace*{-1em}


    \section{Schleifen herum um den Ursprung}

    \vspace*{-1.5em}

    \enlargethispage{4\baselineskip}

    \begin{aufgabe}\label{ex:windinghomotopies}
        \vspace*{-0.5em}
        Welche der Schleifen aus Abbildung~\ref{fig:exwinding} kannst du ineinander verformen, ohne die Schleife durch den speziell markierten Punkt -- den \emph{Ursprung} -- hindurch zu bewegen? Probiere es aus, indem du es mit einer Flasche als Ursprung und einer Schnur als Schleife nachstellst und versuchst, die Schnur zu einer der anderen Schleifen zu verformen, ohne dabei die Flasche zu überqueren.
    \end{aufgabe}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{geometrie-invarianten/loops-winding.pdf}
        \caption{Beispiele für Schleifen.}\label{fig:exwinding}
    \end{figure}

    \begin{aufgabe}\label{ex:windingrule}
        Fällt dir eine Regel auf, mit der du bestimmen kannst, ob zwei Schleifen ohne Überquerung des Ursprungs ineinander verformbar sind, ohne es vorher erfolgreich auszuprobieren?
    \end{aufgabe}




    \newpage

    \vspace*{-2em}


    \section{Windungszahl}

    % Folgende praktische Definition wird uns helfen, die Aufgaben~1 und~2 einfacher zu lösen.

    \begin{definition}[Windungszahl um den Ursprung]\label{def:windingzero}
        Die \emph{Windungszahl}~$\omega_0(K) \in \mathbb{Z}$ einer Schleife~$K$ ist die Anzahl an Umdrehungen einer Schleife um den Ursprung~$0$ herum.
    \end{definition}

    \begin{aufgabe}
        Ermittle die Windungszahl jeder Kurve aus Abbildung~\ref{fig:exwinding} um ihren jeweiligen Ursprung -- jeweils markiert durch das Kreuzchen. Was fällt dir auf? % fig:exwinding jeweils zeilenweise von links, Orientierungen ohne Einschränkung so gewählt, dass die Windungszahl positiv ist. Zeile 1: 1, 3, 2, 0, 2. Zeile 2: 2, 0, 3, 1. Zeile 3: 1, 0, 2
    \end{aufgabe}

    Ab jetzt dürfen Schleifen beim Verformen auch durch den Ursprung bewegt werden.

    \begin{aufgabe}
        Seien $K_1$ und $K_2$ zwei Schleifen mit ungleicher Windungszahl um den Ursprung. Wenn wir $K_1$ zu $K_2$ verformen, muss sich also irgendwann die Windungszahl ändern. Findest du heraus, unter welchen Bedingungen sich die Windungszahl während der Verformung ändert? % wenn bei der Verformung der Ursprung überquert wird.
    \end{aufgabe}




    \section{Zusammenhangskomponenten}

    Bei der Analyse von Schleifen können wir nicht nur die Schleife selbst und ihre Schnittpunkte betrachten, sondern auch die Teile, in die die Schleife die Ebene zerteilt. Wir nennen diese Teile \emph{Zusammenhangskomponenten}.

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.4\linewidth]{geometrie-invarianten/kol-leoniecomps1.pdf}
        \hspace*{5em}
        \includegraphics[width=0.4\linewidth]{geometrie-invarianten/kol-leonie-dots.pdf}
        \caption{Schleife links: gefärbte Zusammenhangskomponenten; rechts: ausgewählte Punkte.}\label{fig:loopleo}
    \end{figure}

    \begin{aufgabe}
        Zähle für die Schleife in Abbildung~\ref{fig:loopleo} die Anzahl der Zusammenhangskomponenten. % 10 Zusammenhangskomponenten (die unbeschränkte nicht vergessen!)

        \textbf{Bonus:} Wie viele Zusammenhangskomponenten hat eine Schleife $K ,$ wenn alle ihre Schnittpunkte nur Doppelpunkte sind -- also z.B. keine dreifachen Schnittpunkte -- und sie $n_K$ viele Schnittpunkte hat? Teste dein Ergebnis auch an den Schleifen aus Abbildung~\ref{fig:exwinding}.\\ % Anzahl an Zshk'en ist gleich n_K + 2
        Wie könntest du mit mehr als doppelten Schnittpunkten klarkommen? % Seien n_j die j-fachen Doppelpunkte einer Schleife. Dann ist die Anzahl der Zshk'en gleich 2 + \sum \limits_{j=2}^{\infty} n_j (j - 1) -> d.h. wir haben 2 Komponenten von Anfang an, und dazu pro j-fachem Doppelpunkt (j-1) viele Komponenten zusätzlich.
    \end{aufgabe}


    Jetzt sollten wir den Begriff der Windungszahl auch auf beliebige Punkte ausweiten.

    \begin{definition}[Windungszahl um beliebigen Punkt]
        Die Windungszahl~$\omega_p(K)$ einer Schleife~$K$ ist die Anzahl an Umdrehungen einer Schleife um einen beliebigen Punkt~$p \notin K$ herum.
    \end{definition}

    \begin{aufgabe}
        Bestimme die Windungszahl für jeden Punkt im rechten Bild von Abbildung~\ref{fig:loopleo}. % von oben nach unten: 1, -1, 2, 0
    \end{aufgabe}

    \enlargethispage{3\baselineskip}

    Nach kurzer Überlegung stellen wir fest, dass die Windungszahl für alle Punkte innerhalb einer Zusammenhangskomponente gleich ist.
    Dadurch ist es sinnvoll, ganzen Zusammenhangskomponenten jeweils eine Windungszahl zuzuschreiben.

    \begin{aufgabe}
        Bestimme die Windungszahl für jede Zusammenhangskomponente links in Abbildung~\ref{fig:loopleo}. % die blauen haben 1, die orangene -1, die pinke 0, die weiße auch 0, die grünen 2.
    \end{aufgabe}





    \section{Querungen-Trick (für Windungszahlen)}

    \begin{figure}[h!]
        \centering
        \includegraphics[width=0.6\linewidth]{geometrie-invarianten/windingstep.pdf}
    \end{figure}
    
    Beim Überqueren eines Kurvenstücks unterscheidet sich die Windungszahl der anliegenden Komponenten immer genau um $1$ oder $-1$, abhängig von der Orientierung des Kurvenstücks.


    \section{Einfache Anwendung: Punkt-in-Polygon (PIP)}

    % Wikipedia: In computational geometry, the point-in-polygon (PIP) problem asks whether a given point in the plane lies inside, outside, or on the boundary of a polygon. It is a special case of point location problems and finds applications in areas that deal with processing geometrical data, such as 
    % - computer graphics,
    % - computer vision,
    % - geographic information systems (GIS),
    % - motion planning,
    % - computer-aided design (CAD).
    % https://en.wikipedia.org/wiki/Point_in_polygon
    

    \begin{figure}[h!]
        \centering
        \includegraphics[width=\linewidth]{geometrie-invarianten/pip.pdf}
        % \caption{Einfache Beispiele für das PIP-Problem.}
    \end{figure}


%     \section{Rotationszahl und reguläre Verformungen}

%     \enlargethispage{2\baselineskip}

%     \begin{definition}[Rotationszahl]
%         Die \emph{Rotationszahl}~$\operatorname{rot}(K) \in \mathbb{Z}$ einer Schleife~$K$ ist die Anzahl an Umdrehungen des Geschwindigkeitsvektors beim einmaligen Durchlaufen der Schleife.
%     \end{definition}

%     \begin{hinweis}
%         \, \vspace*{-0.25em}
%         \begin{itemize}
%             \item Die Drehungen werden im üblichen mathematischen Drehsinn des zweidimensionalen Koordinatensystems gezählt. \vspace*{-0.25em}
%             \item Die Rotationszahl kann auch für nicht geschlossene Kurven definiert werden. \vspace*{-0.25em}
%             \item Die Rotationszahl ist nur definiert für \textbf{Kurven ohne (versteckte) Ecken}. Solche Kurven nennen wir auch \emph{reguläre Kurven}.
%         \end{itemize}
%     \end{hinweis}

%     \begin{aufgabe}
%         \vspace*{-1em}
%         \begin{enumerate}[a)]
%             \item Bestimme die jeweilige Rotationszahl für die Kurven aus Abbildungen~\ref{fig:exwinding} und~\ref{fig:loopleo}. % fig:exwinding jeweils zeilenweise von links, Orientierungen ohne Einschränkung so gewählt, dass die Rotationszahl positiv ist. Zeile 1: 0, 3, 2, 1, 2. Zeile 2: 2, 0, 3, 2. Zeile 3: 1, 0, 2; fig:loopleo hat Rotationszahl 3
%             \item An welchen Stellen ändert sich die Rotationszahl einer Kurve, wenn wir sie verformen? % bei Ecken durch die Loopings entstehen oder verschwinden
%             \item Unter welchen Bedingungen haben zwei Schleifen die gleiche Rotationszahl? % wenn sie regulär homotop sind, also wenn sie regulär ineinander verformbar sind (siehe unten)
%         \end{enumerate}
%     \end{aufgabe}




%     \newpage

%     \begin{definition}[Reguläre Verformung]
%         Wir nennen die Verformung von einer Schleife zu einer anderen \emph{regulär}, wenn während der Verformung zu keinem Zeitpunkt Ecken existieren.
%     \end{definition}

%     Bei einer regulären Verformung können also keine Loopings entstehen oder verschwinden.\\
%     \textbf{Bonus:} Das ist die erste der drei \emph{Reidemeister-Bewegungen}.

%     \begin{satz}[\textbf{Bonus:} Satz von Whitney--Graustein]
%         Zwei Schleifen haben genau dann die gleiche Rotationszahl, wenn sie regulär ineinander verformt werden können.
%     \end{satz}

%     \begin{hinweis}
%         Die Rotationszahl ist Grundlage vieler anderer geometrischer Kennzahlen.
%         Ohne reguläre Verformungen und ihre Eigenschaften (v.a. der \emph{Satz von Whitney--Graustein}) würden so gut wie alle Theoreme rund um reguläre Kurven einfach nicht funktionieren beziehungsweise nicht beweisbar sein.
%         Eine praktische Anwendung ist zum Beispiel die $J^+$-Invariante, die Anhaltspunkte dafür gibt, ob ein im Weltraum umherfliegender Satellit ohne Kollision mit einem Planeten seinen Orbit zu einem neuen Wunschorbit ändern kann.
%     \end{hinweis}

%     \begin{hinweis}
%         Was wir im ganzen Zirkel \emph{Verformung} genannt haben, nennt man üblicher auch \emph{Homotopie}. Kurven werden dann \emph{homotopiert}, statt \emph{verformt}. Und die Rotationszahl einer Schleife bleibt gleich unter \emph{regulärer Homotopie}.
%     \end{hinweis}



%     \section{Lächeln-Trick (für Rotationszahlen)}

%     Zähle jedes richtige und umgedrehte Lächeln, die beim Durchlaufen der Kurve nach rechts (oder links) auftreten. Die Differenz ist die Rotationszahl (bis auf Vorzeichenfehler).

%     \begin{figure}[h!]
%         \centering
%         \includegraphics[width=0.5\linewidth]{geometrie-invarianten/kol-leonie-rot-smiles-frowns.pdf}
%         % \caption{}
%     \end{figure}
%     \vspace*{-1em}

%     Ein anderer einfacher Trick, um die Rotationszahl einer komplizierten Kurve herauszufinden, ist es sie zu einer einfacheren Kurve regulär zu verformen. Da die Rotationszahl sich beim regulären Verformen nicht ändert, können wir dann einfach die Rotationszahl der einfacheren Kurve ermitteln.

%     \vspace*{3em}
%     \noindent\fbox{%
%     \parbox{\textwidth}{%
%         \textbf{Schauempfehlung} zum Thema Knotentheorie (leider nur englisch, ohne deutsche Untertitel): ``\emph{How The Most Useless Branch of Math Could Save Your Life}'' von \emph{Veritasium}, auf YouTube.
%     }%
% }
    
%     % Für Fortsetzung: Reidemeister-Bewegungen und anderer Kram aus https://www.youtube.com/watch?v=8DBhTXM_Br4 evtl auch bisschen J+ falls spaßig möglich


\end{document}